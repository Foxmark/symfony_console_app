#!/usr/bin/php
<?php

namespace Foxmark;

require_once 'vendor/autoload.php';

use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new Stock());
$app->run();
