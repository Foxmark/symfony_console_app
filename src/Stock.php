<?php

namespace Foxmark;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;


class Stock extends Command
{
    protected static $defaultName        = 'stock';
    protected static $defaultDescription = 'Update Stock or something.';

    protected function configure()
    {
        $this->setHelp('Update Stock for some reason.')
            ->addArgument('SKU', InputArgument::REQUIRED, 'Product ID')
            ->addOption('format', 'f', InputOption::VALUE_OPTIONAL, 'Some format');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $format      = $input->getOption('format');

        if($format) {
            $output->writeln('<comment>Selected Format: ' . $format . '</comment>');
        }

        $choices = [
            1 => 'Apples',
            2 => 'Oranges'
        ];

        $question = new ChoiceQuestion('Pick one: ', $choices);

        $helper   = $this->getHelper('question');
        $selected = $helper->ask($input, $output, $question);

        $output->writeln('You want: ' . $selected);

        $progressBar = new ProgressBar($output, 100);

        $progressBar->start();

        $i = 0;
        while ($i++ < 50) {
            // ... do some work

            // advances the progress bar 1 unit
            //$progressBar->advance();
            usleep(15000);

            // you can also advance the progress bar by more than 1 unit
            $progressBar->advance(2);
        }

        // ensures that the progress bar is at 100%
        $progressBar->finish();
        $output->writeln('');

        // ask question
        $helper   = $this->getHelper('question');
        $question = new Question('<question>Please enter your name:</question> ', 'Noname');
        $bundleName = $helper->ask($input, $output, $question);

        // show confirmation
        $helper   = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action '.$bundleName.'? [y/N]', false);

        if($helper->ask($input, $output, $question)) {
            $output->writeln('<comment>Stock for SKU: ' . $input->getArgument('SKU') . '</comment> = <error>' . rand(100, 999) . '</>');
        }
        return Command::SUCCESS;
    }
}